## @namespace ht.setup_time
# Reads hydi's output and extracts the setup time from it
#
# Reads the file hydi_output.txt (the redirected standard output) for the line containing "runtime for setup" and extracts the time from it
def setuptime():
    with open("run/hydi_output.txt","r") as file:
        for line in file:
            if "runtime for setup" in line:
                linepart = line.split() #Split line on white spaces
                hydisetuptime = float(linepart[3])
    return hydisetuptime
