## @namespace ht.filter_results
# Filters the nosetests output into a more readable format
#
# Accepts a command line argument 'filename' as the name of a file to be filtered. Reads the file and saves some lines to an array to be written into the file "filtered_'filename'".\n
# A line is removed if it contains
# * testtools
# * File
# * postfix_content
# * }}}
# * Failed expectation
# * Traceback
# * AssertionError
#
# If the line contains 'self.expectThat' everything on the line is stripped so only the function name remains. The function name is added to the array

import argparse

def filterresults():

    text = []

    ##Read in a command line argument as the file that will be filtered
    parser = argparse.ArgumentParser()
    parser.add_argument('filename',help ="The name of the file that you want filtered")
    inputfile = parser.parse_args()

    with open(inputfile.filename,"r") as file:
        for line in file:
            if ("testtools" in line) == True:
                continue
            elif ("File" in line) == True:
                continue
            elif ("self.expectThat" in line) == True:
            #These operations allow us to only append the function name to the array
            #Temporary variable to make the code easier to read
                temp = line.split("self.expectThat(")[1]
                temp = temp.replace (" ","")
                text.append(temp.split(")")[0] + ") ")
            elif ("postfix_content" in line) == True:
                continue
            elif ("}}}" in line) == True:
                continue
            elif ("Failed expectation" in line) == True:
                continue
            elif ("Traceback" in line) == True:
                continue
            elif ("AssertionError" in line) == True:
                continue
            #I thought skipping the blank lines made it harder to read
            #Skip blank lines
    #            elif not line.strip():
    #                continue
            else:
                text.append(line)

    with open("filtered_" + inputfile.filename,"w") as file:
        for line in text:
            file.write(line)

    print("filtered_%s file created"%inputfile.filename) 

#This allows the function to only be executed if you run the file, not when you import it
if __name__ == '__main__':
    filterresults()
