## @namespace ht.run_time
# Reads hydi's output and extracts the run time from it
#
# Reads the file hydi_output.txt (the redirected standard output) for the line containing "runtime for time step" and extracts the value from it. Each time a run time is found, it is saved over the previous one so the last value is always returned
def runtime():
    with open("run/hydi_output.txt","r") as file:
        for line in file:
            if "runtime for time step" in line:
                linepart = line.split()   #Split line on white spaces
                hydiruntime = linepart[4] 
    return float(hydiruntime)
