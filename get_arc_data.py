## @namespace ht.get_arc_data
# Reads the file h2u_network-data.txt and extracts the data from it
#
# * If the iteration of hydi is passed to the function, the iteration is appended to the file name so we can compare the current iteration of hydi to a specific reference output file
# * Reads the file 'h2u_network-data.txt' and extracts the face value from it

import os

## @param path      Path to the file 'h2u_network-data.txt'
## @param facename  The name of the face being compared
## @param iteration If specified contains the current run of hydi
def getarcdata(path,facename,iteration = -1):   

    #Check if we need to read different input files for each iteration of hydi
    if iteration == -1:
        filename = "h2u_network-data.txt"
    else:
        filename = "h2u_network-data_" + str(iteration) + ".txt"

    with open(os.path.join(path,filename), "r") as file:
        #Split the first line on white spaces and read into array firstline
        firstline = file.readline().split()
        column = 0
        for element in firstline:
            if facename == element:
                #We want to know what column the face data is in. The first 2 columns are always time and mg.fld.ener so we need to add 2 to skip those columns
                column += 2
                break
            column += 1
        for line in file:
        #Split line on commas into array row
            row = line.split(",")
    #Strip removes leading and trailing spaces from a string
    return float(row[column].strip()) 
