## @namespace ht.hydi
# Contains functions related to operating hydi

import os
import re
import shutil
import subprocess
import sys
import time

##Contains functions related to operationg hydi
#
# * Deletes the old 'run' directory if it exists and creates a new one when the hydi object is created
# * If there is different input files for each iteration of hydi Hydi.renameinputfiles will rename them to the correct name
# * Hydi.start starts hydi
# * Hydi.run runs one static iteration of hydi
# * Hydi.runDynamic runs one dynamic iteration of hydi
# * When the object is destroyed hydi's __exit__ statement calls Hydi.stop to stop Hydi 
class Hydi:
    #Self is used instead of requiring declarations like in C++
    #Self is like C++'s 'this'
    #http://stackoverflow.com/questions/2709821/python-self-explained
    
    ## The constructor
    #
    # Checks if a directory named 'run' exists. If so, deletes it. It then copies the directory 'input' to the directory 'run' \n
    #Note: __init__ is not actually a constructor since it isn't executed until after the object has been created. This is the closest python has to C++'s constructor.

    ##
    # @param self Refers to the current instance of the class \n
    # Note: self is not a reserved keyword, it is only a convention
    def __init__(self):
        
        ## @param hydicanstart True: allows hydi to start, False: does not
        self.hydicanstart = True

        #Checks if the directory "run" exists and deletes it if so
        if os.path.exists("run"):
            shutil.rmtree("run")

        #Looks for the input folder "input" and throws an error if it is not found
        if not os.path.exists("input"):
            print("Tried to copy input. Looked in directory")
            print(os.getcwd())
            sys.exit()
        #Creates a folder called run and copies the input files into it
        else:
           shutil.copytree("input","run")

    ## Allows us to create a Hydi object using a with statement
    # @param self Refers to the current instance of the class
    def __enter__(self):
        return self
    ## Renames the 'iteration'th file to the name hydi can read
    #
    # Hydi has specific names for the files that it reads but we also want to have the flexibility of having different input files at different iterations. The files are named "filename_iteration.mhdx" but hydi reads the file "filename.mhdx". To solve this we use a regular expression to rename the files. 

    ## @param self      Refers to the current instance of the class
    ## @param iteration The current run of hydi
    def renameinputfiles(self,iteration):       
    #matches any string followed by "_", then the iteration number, followed by suffix. If the file matches this expression we rename it to the hydi readable version.
        suffixes=['mhdx','inp','txt']
        for s in suffixes:
            exp = r".*_" + str(iteration) + "\." + s
            for file in os.listdir("run"):
                if bool(re.match(exp,file)) == True:
                    shutil.copy("run/"+file,("run/"+file.replace("_" + str(iteration) + ".",".")))
            if os.path.exists("run/temp"):
                for file in os.listdir("run/temp"):
                    if bool(re.match(exp,file)) == True:
                        shutil.copy("run/temp/"+file,("run/temp/"+file.replace("_" + str(iteration) + ".",".")))
           
    ## Starts Hydi
    #
    # - Deletes the control file if it exists
    # - Changes to the run directory
    # - Checks if this instance of hydi is running. If it isn't,hydi starts. If hydi is running it prints to the screen that hydi is currently running\n
    # - Standard output is redirected to hydi_output.txt and standard error to hydi_error.txt\n
    # - Changes back to the current case' directory
    # - Waits for the control file to exist
    # - Waits for hydi to write a 0 into it 
    # @param self Refers to the current instance of the class
    def start(self):
        #Change to the run directory
        os.chdir("run")

        if self.hydicanstart == True:
        #Using Popen starts hydiBreaker in the background and redirects standard output to file called hydi_output.txt and standard error to hydi_error.txt        
            self.hydiproc = subprocess.Popen([os.path.join("..","..","..","hydiBreaker")],stdout=open("hydi_output.txt","w"),stderr=open("hydi_error.txt","w"))

            self.hydicanstart = False
        else:
            print("Cannot start hydi. Current instance of hydi is already running.")

        #Change the directory up a level to the current cases' directory
        os.chdir("..")

        #Check every half second if the control file exists
        while  os.path.isfile(os.path.join("run","control_file.txt")) == False and os.path.isfile(os.path.join("run","temp","control_file.txt")) == False:
            time.sleep(0.1)

        if os.path.isfile(os.path.join("run","temp","control_file.txt")):
            self.controlfilepath = os.path.join("run","temp","control_file.txt")
        else:
            self.controlfilepath = os.path.join("run","control_file.txt")

        #Wait for hydi to write a 0 to the control file 
        #Note: Opened the file like this instead of using a with statement because it doesn't read the file correctly if a with statement is used
        file = open(self.controlfilepath,"r")
        line = file.readlines()
        while line[0].startswith("0") == False:
            time.sleep(0.1)
            line = file.readlines()
        file.close()
        

        
    #Check if hydi has exited due to some error
    def checkIfRunning(self):
    #Will return None if hydi is still running. Will return -N indicating the child was terminated by process N
        if self.hydiproc.poll() != None:        
            #Need to see if a break statement would be okay if throwing an exception is needed
            raise Exception("Hydi was terminated")
            
    ## Runs a static iteration of Hydi
    #
    # Writes a 1 to the control file and then waits for Hydi to write a 0 into it
    # @param self Refers to the current instance of the class
    def run(self):
        with open(self.controlfilepath,"w") as file:
            file.write("1")

        #Wait for hydi to write a 0 to the control file so we know it is done creating the files to compare
        line = []
        #If python reads the control file at the exact moment hydi is writing to it line will equal [] (An empty list). If it then goes to check if it starts with 0, an IndexError will be thrown saying "list index out of range". Python supports short-circuiting so we can check if line is empty or line starts with 0 to avoid the error
        while (line == [] or line[0].startswith("0") == False):
            file = open(self.controlfilepath,"r")
            time.sleep(0.1)
            self.checkIfRunning()
            line = file.readlines()
            file.close() 
    
    ## Runs a dynamic iteration of Hydi
    #
    # Writes a 5 to the control file and then waits for Hydi to write a 0 into it
    # @param self Refers to the current instance of the class
    def runDynamic(self):
        with open(self.controlfilepath,"w") as file:
            file.write("5")

        #Wait for hydi to write a 0 to the control file so we know it is done creating the files to compare
        line = []
        #If python reads the control file at the exact moment hydi is writing to it line will equal [] (An empty list). If it then goes to check if it starts with 0, an IndexError will be thrown saying "list index out of range". Python supports short-circuiting so we can check if line is empty or line starts with 0 to avoid the error
        while (line == [] or line[0].startswith("0") == False):
            file = open(self.controlfilepath,"r")
            time.sleep(0.1)
            self.checkIfRunning()
            line = file.readlines()
            file.close() 
 
    ## Restarts Hydi at given time level
    #
    # Writes a -1 and the given time level to the control file and then waits for Hydi to write a 0 into it
    # @param self Refers to the current instance of the class
    # @param timeLevel The time level from which to restart
    def restart(self,timeLevel):
        with open(self.controlfilepath,"w") as file:
            file.write("-1 %s" % timeLevel)

        #Wait for hydi to write a 0 to the control file so we know it is done creating the files to compare
        line = []
        #If python reads the control file at the exact moment hydi is writing to it line will equal [] (An empty list). If it then goes to check if it starts with 0, an IndexError will be thrown saying "list index out of range". Python supports short-circuiting so we can check if line is empty or line starts with 0 to avoid the error
        while (line == [] or line[0].startswith("0") == False):
            file = open(self.controlfilepath,"r")
            time.sleep(0.1)
            self.checkIfRunning()
            line = file.readlines()
            file.close()

    ## Stops Hydi
    #
    # Writes a 2 in the control file
    # @param self Refers to the current instance of the class
    def stop(self):
        with open(self.controlfilepath,"w") as file:
            file.write("2")

        self.hydicanstart = True

    ##The 'destructor'
    #
    # Calls the stop function
    # @param self Refers to the current instance of the class
    # @param type The type of exception
    # @param value The exception instance raised
    # @param traceback A traceback instance
    def __exit__(self,type,value,traceback):
        self.stop()
