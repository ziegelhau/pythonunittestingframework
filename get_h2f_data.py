## @namespace ht.get_h2f_data
# Reads the file h2f_results.mhdx and extracts the h2f data from it
#
# * If the iteration of hydi is passed to the function, the iteration is appended to the file name so we can compare the current iteration of hydi to a specific reference output file
# * Goes through the h2f_results.mhdx and saves each element of the h2f parameter into an array.

import os
import numpy as np
import sys

## @param path      Path to the file 'h2f_results.mhdx'
## @param parameter The name of the property being compared(i.e either current, losses, forces or B)
## @param iteration If specified it contains the current run of hydi
def geth2fdata(path,parameter,iteration = -1):

    #Check if we need to read different input files for each iteration of hydi
    if iteration == -1:
        filename = "h2f_results.mhdx"
    else:
        filename = "h2f_results_" + str(iteration) + ".mhdx"

    #Convert parameter to all lowercase characters
    parameter = parameter.lower()
    #Check what the value of parameter is
    if parameter == "current" or parameter == "currents":
        startcolumn = 1
        endcolumn = 4
        arraycolumns = endcolumn - startcolumn

    elif parameter == "losses":
        startcolumn = 4
        endcolumn = 5
        arraycolumns = endcolumn - startcolumn

    elif parameter == "force" or parameter == "forces":
        startcolumn = 5
        endcolumn = 8
        arraycolumns = endcolumn - startcolumn

    elif parameter == "b" or parameter == "magnetic" or parameter == "magnetic-filed":
        startcolumn = 8
        endcolumn = 11
        arraycolumns = endcolumn - startcolumn
    else: 
        raise NameError("The input parameter was not current, losses, force, or B. Check the spelling of you arguments in the compareh2f function")

    with open(os.path.join(path,filename), "r") as file:
        #I don't know if there is a better way to write code to find the second occurance of something.  It reminds me of unlocking doors to get to the next one
        unlockdoor = 0
        for line in file:
            if line.startswith("Table Lines") == True:
                if unlockdoor == 1:
                    #Split on white spaces
                    lineparts = line.split()
                    tablelines = int(lineparts[2]) 
                    file.readline()
                    row = 0
                    break
                unlockdoor += 1

        data = np.zeros((tablelines,arraycolumns))    

        #Now that we know the table is starting and how many lines are in it we can extract the data
        for line in file:
            if row >= tablelines:
                break            
            lineparts = line.split()
            column = 0
            for element in range(startcolumn,endcolumn):
                data[row,column] = lineparts[element]
                column += 1
            row += 1
    return data
