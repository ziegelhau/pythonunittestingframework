## @namespace ht.old_get_surface_data
# Reads the file 'surface_currents.txt' and extracts the data from it
#
# * If the iteration of hydi is passed to the function, the iteration is appended to the file name so we can compare the current iteration of hydi to a specific reference output file
# * Goes through the file and saves the surface current data into an array 

import os
import numpy as np

## @param path      The path to the file 'surface_currents.txt'
## @param iteration If specifed it contains the current run of hydi
def oldgetsurfacedata(path, iteration = -1):

    if iteration == -1:
        filename = 'surface_currents.txt'
    else:
        filename = 'surface_currents' + "_" + str(iteration) + ".txt"

    vector = np.array([0],float)
    row = 0

    with open(os.path.join(path,filename), "r") as file:
        for line in file:
            numbers = line.split() #Split the line on whitespaces
            vector.resize(row + 1)
            vector[row] = numbers[1] #save the second column to a vector
            row += 1
    return vector
