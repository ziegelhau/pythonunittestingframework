## @namespace ht.arj_max_iter
# Reads hydi's output and extracts the maximum arc root jump iteration from it
#
# Reads the file hydi_output.txt (the redirected standard output) for the line containing ' it ' (including the spaces) and extracts the value from it: This is the maximum arc root jump iteration. Each time an iteration is found, it is saved over the previous one so the last value is always returned.

def arjmaxiter():
    with open("run/hydi_output.txt","r") as file:
        for line in file:
            #Note the spaces
            if " it " in line:
                linepart = line.split() #Split line on white spaces
                hydiarjmaxiter = linepart[1]
    return float(hydiarjmaxiter) 
