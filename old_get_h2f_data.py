## @namespace ht.old_get_h2f_data
# Reads the h2f file and extracts the data from it
#
# * Decides which file to compare based on the file name. This is necessary because the mhdx format only specifies what parameter to compare. Ex: 'currents' instead of 'h2f_currents.out'
# * If the iteration of hydi is passed to the function, the iteration is appended to the filename so we can compare the current iteration of hydi to a specific reference output file 
# * Goes through the file and saves each x, y, and z element into column 1,2, and 3 respectively of an array. The cell identifier is skipped as well as the file header

import numpy as np
import os

## @param path      The path to the file
## @param attribute The name of the parameter being compared
## @param iteration If specified it contains the current run of hydi 
def oldgeth2fdata(path,attribute,iteration = -1):

    attribute = attribute.lower()
    if attribute == "current" or attribute == "currents":
        filename = "h2f_currents"

    elif attribute == "losses":
        filename = "h2f_losses"

    elif attribute == "force" or attribute == "forces":
        filename = "h2f_forces"

    elif attribute == "b" or attribute == "magnetic" or attribute == "magnetic-filed":
        filename = "h2f_magneticfield"
    else:
        filename = attribute    
    #Check if a file was specified or if we need to add ".out" to complete the name 
    if ".out" in filename:
        pass
    #Check if we need to read different input files for each iteration of hydi
    else:
        if iteration == -1:
            filename = filename + ".out"
        else:
            filename = filename + "_" + str(iteration) + ".out"    

    with open(os.path.join(path,filename), "r") as file:
        next(file) #Skips reading the first line
        vector = np.array([0,0,0],float)
        row = 1
        for line in file:            
            column = 0
            numbers = line.split() #Split the line on whitespaces
            for element in numbers:
                #Use an if statement instead of setting column = 1 on each for loop so the first column is not added to the vector
                if column == 0: #Skips the first column of text
                    column += 1
                    vector.resize(row,3) #Add a row(on the first iteration it riesizes it to a 1,3 array
                    continue
                vector[row - 1,column - 1] = element 
                column += 1
            row += 1
    return vector
