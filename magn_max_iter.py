## @namespace ht.magn_max_iter
# Reads hydi's output and extracts the maximum magnetic iteration from it
#
# Reads the file hydi_output.txt (the redirected standard output) for the line containing "nonlinear iter" and extracts the value from it: This is the 'maximum magnetic iteration'. Each time an iteration is found, it is saved over the previous one so the last value is always returned.
def magnmaxiter():
    with open("run/hydi_output.txt","r") as file:
        for line in file:
            if "nonlinear iter" in line:
                linepart = line.split()   #Split line on white spaces
                hydimagnmaxiter = linepart[2] 
                hydimagnmaxiter = hydimagnmaxiter[:-1]
    return float(hydimagnmaxiter) 
