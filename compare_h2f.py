## @namespace ht.compare_h2f
# Calculates the relative error between test and reference for h2f results
#
# * Checks whether the new or old format is being used
# * Calls the appropriate geth2fdata function to extract the data from the file
# * Calculates the maximum relative error between test and reference.\n\n
#
# <center> The mathematics to calculate the relative error with reference as 1 and test as 2
# \f[
#    e_r=\left|
#         \frac{max \sqrt{(x_1-x_2)^2+(y_1-y_2)^2+(z_1-z_2)^2}}
#         {max \sqrt{x_1^2+y_1^2+z_1^2}}
#        \right|
# \f]

import numpy as np
import os
from old_get_h2f_data import oldgeth2fdata
from get_h2f_data import geth2fdata

#Note 1: By default NumPy does it's matrix multiplication elementwise
## @param attribute The name of the property being compared(i.e either current, losses, forces or B) 
## @param iteration -1 if old version or hydi iteration value if new version
def compareh2f(attribute,iteration = -1):
        
        #Check if we need to compare the oldest format or the newer one
        #The file h2f_results.mhdx is only generated with the newer format so we know to use the new function if this file exists
    if os.path.exists(os.path.join("run","temp","h2f_results.mhdx")) == True: 
        testresults   = geth2fdata("run/temp"  ,attribute)
        referenceresults = geth2fdata("ref_output",attribute,iteration)
    else:
        testresults      = oldgeth2fdata("run"       ,attribute)
        referenceresults = oldgeth2fdata("ref_output",attribute,iteration)

    difference = referenceresults - testresults

    refsquared = referenceresults**2
    difsquared = difference**2
 
    refrowsum = refsquared.sum(axis=1)
    difrowsum = difsquared.sum(axis=1) 
    
    difmax = np.amax(difrowsum)
    refmax = np.amax(refrowsum)    

    #Catch 0/0 division here. Allow (any number)/0 because that error should be seen
    if difmax == 0 and refmax  == 0:
        maxrelerr = 0
    else:
        maxrelerr = np.sqrt(difmax/refmax)
    
    return maxrelerr
