## @namespace ht.compare_arc
#Calculates the relative error between hydi and hadapt for the file 'h2u_network-data.txt'
#
# * Calls the getarcdata function to get the data from the 'h2u_network-hata.txt' files
# * Calculates the maximum relative error between hydi and hadapt.\n\n
#
# <center> The mathematics to calculate the relative error with Hadapt as 1 and Hydi as 2
# \f[ e_r = \left| \frac{x_1-x_2}{x_1} \right| \f]

import os
import numpy as np
from get_arc_data import getarcdata

## @param facename  The name of the face being compared
## @param iteration If specified contains the current run of hydi
def comparearc(facename,iteration = -1):

    if os.path.exists(os.path.join("run","HADAPT_results")) == True:
        hydiresults   = getarcdata(os.path.join("run","HADAPT_results"),facename)
    else:
        hydiresults   = getarcdata(os.path.join("run","hydi_results"),facename)
    if iteration == -1:
        hadaptresults = getarcdata(os.path.join("ref_output"),facename)
    else:
        hadaptresults = getarcdata("ref_output",facename,iteration)

    difference = hadaptresults - hydiresults
    
    #Catch 0/0 division here. Allow (any number)/0 because that error should be seen 
    if difference == 0 and hadaptresults == 0:
        relativeerror = 0
    else:
        relativeerror = np.absolute(difference/hadaptresults)
    
    return relativeerror
