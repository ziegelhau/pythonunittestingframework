## @namespace ht.cur_cons_error
# Reads hydi's output and extracts the current conservation error from it
#
# Reads the file hydi_output.txt (the redirected standard output) for the line containing 'current conservation error' and extracts the value from it. Each time a value is found, it is saved over the previous one so the last value is always returned

def curconserror():
    with open("run/hydi_output.txt","r") as file:
        for line in file:
            if "current conservation error" in line:
                linepart = line.split() #Split line on white spaces
                curconserr = float(linepart[4])
    return curconserr
