## @namespace ht.get_surface_data
# Reads the file h2f_results.mhdx and extracts the surface data from it
#
# * If the iteration of hydi is passed to the function, the iteration is appended to the file name so we can compare the current iteration of hydi to a specific reference output file
# * Goes through the h2f_results.mhdx and saves each element of the current into an array

import os
import numpy as np
import sys

## @param path      Path to the file 'h2f_results.mhdx'
## @param iteration If specified it contains the current run of hydi
def getsurfacedata(path,iteration = -1):

    #Check if we need to read different input files for each iteration of hydi
    if iteration == -1:
        filename = "h2f_results.mhdx"
    else:
        filename = "h2f_results_" + str(iteration) + ".mhdx"

    with open(os.path.join(path,filename), "r") as file:
        #I don't know if there is a better way to write code to find the third occurance of something.  It reminds me of unlocking doors to get to the next one               
        unlockdoor = 0
        for line in file:
            if line.startswith("Table Lines") == True:
                if unlockdoor == 2:
                    #Split on white spaces
                    lineparts = line.split()
                    tablelines = int(lineparts[2]) 
                    file.readline()
                    row = 0
                    break
                unlockdoor += 1

        data = np.zeros((tablelines))    

        #Now that we know the table is starting and how many lines are in it we can extract the data
        for line in file:
            if row >= tablelines:
                break            
            lineparts = line.split()
            data[row] = lineparts[1]
            row += 1
    return data
