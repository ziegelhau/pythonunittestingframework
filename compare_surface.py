## @namespace ht.compare_surface
# Calculates the relative error between test and reference for the current in the h2f_results.mhdx file
#
# * Checks whether the new or old format is being used
# * Calls the appropriate getsurfacedata function to get data from the file
# * Calculates the maximum relative error between test and reference.\n\n
#
# <center> The mathematics to calculate the relative error with reference as 1 and test as 2
# \f[ e_r = \left| \frac{max(x_1-x_2)}{max(x_1)} \right| \f]

import os
import numpy as np
from old_get_surface_data import oldgetsurfacedata
from get_surface_data import getsurfacedata

## @param iteration If specified it contains the current run of hydi
def comparesurface(iteration = -1):
        #Check if we need to compare the old format or the new one
        #The file h2f_results.mhdx is only generated with the new format so we know to use the new function if this file exists
    if os.path.exists(os.path.join("run","temp","h2f_results.mhdx")) == True:
        testresults = getsurfacedata("run/temp")
        referenceresults = getsurfacedata("ref_output",iteration)
    else:
        testresults = oldgetsurfacedata("run")
        referenceresults = oldgetsurfacedata("ref_output",iteration)

    difference = referenceresults - testresults
    
    difmax = np.amax(np.absolute(difference))
    refmax = np.amax(np.absolute(referenceresults))

    #Catch 0/0 division here. Allow (any number)/0 because that error should be seen
    if difmax == 0 and refmax  == 0:
        maxrelerr = 0
    else:
        maxrelerr = difmax/refmax
    
    return maxrelerr
