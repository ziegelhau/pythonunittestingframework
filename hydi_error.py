## @namespace ht.hydi_error
# Returns the content of the standard error file
#
# Returns the text in the file 'hydi_error.txt'

def hydierror():
    with open("run/hydi_error.txt","r") as file:
        filetext = []
        for line in file:
            filetext.append(line)           
    return filetext
