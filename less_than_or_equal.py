## @namespace ht.less_than_or_equal
# Custom testools matcher
#
# Does a less than or equal comparison

##Called if LessThanOrEqual fails
#
#Generates the failure message for LessThanOrEqual
class MisMatch:
    ##The Constructor
    # @param self Refers to the current instance of the class
    # @param input The number being tested 
    # @param reference The number being compared to
    def __init__(self,input,reference):
        self.input = input
        self.reference = reference
    ##Describes the failure
    # @param self Refers to the current instance of the class
    def describe(self):
        return "%r is not >= %r" % (self.reference,self.input)
    ##Can be used to provide extra data about the failure
    #
    # Returns and empty dictionary. Could be used to return the contents of a log file for a more detailed failure report
    # @param self refers to the current instance of the class
    def get_details(self):
        return {}

## Does a less than or equal comparison
#
# Recieves an input 'input' and compares if it is less than or equal to a reference value 'reference'
class LessThanOrEqual:
    ## The constructor
    # @param self Refers to the current instance of the class
    # @param reference The number being compared to
    def __init__(self,reference):
        self.reference = reference
    ##Displayed on failure by assertThat
    # @param self Refers to the current instance of the class
    def __str__(self):
        return 'LessThanOrEqual(%s)'%(self.reference)
    ##Returns None if match is successful and MisMatch if unsuccessful
    # @param self Refers to the current instance of the class
    # @param input The number being tested 
    def match(self,input):
        if input <= self.reference:
            return None
        else:
            return MisMatch(input,self.reference)
